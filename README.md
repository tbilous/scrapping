# README

Demo application for use forms on remote resources
### Dependence:
* ruby 2.3.1
* rails 5.0.0.1
* **postgresql '>= 9.4'**
* Redis server
### Install
* clone this repo
* ```bundle install```
* ```run rake db:migrate```; ```rake db:test:prepare``` 
### Run
* start redis-server
* start rails server
* start sidekiq
* Open in browser http://localhost:3000
### Test
* ```rspec /spec```

Thank you for fun
