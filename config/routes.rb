require 'sidekiq/web'

Rails.application.routes.draw do
  resource :searches, only: [:new, :create, :show] do
    get '/:id', to: 'searches#show', as: 'search'
    get '/:id/results', action: 'results'
  end

  mount Sidekiq::Web => '/sidekiq'

  root 'searches#new'
end
