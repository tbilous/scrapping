FactoryGirl.define do
  factory :search do
    city 'Чернівці'
    checkin Date.current + 1.day
    checkout Date.current + 2.days
  end
  factory :wrong_search, class: 'Search' do
    city nil
    checkin nil
    checkout nil
  end
end
