require 'acceptance_helper'

# Yes, i know, what this test is not entirely right
feature 'user can find on booking', %q{
  User can to use form for search on ukrainian language
  User see result data
} do
  let!(:search) { attributes_for(:search) }
  describe 'search on home page' do
    before { visit root_path }

    scenario 'with valid attributes' do
      visit root_path

      expect(page).to have_content 'Booking'

      within '#searchBooking' do
        fill_in 'search_city', with: search[:city]
        fill_in 'search_checkin', with: search[:checkin]
        fill_in 'search_checkout', with: search[:checkout]

        page.find('#searchSubmit').click
      end

      expect(page).to have_content 'Чернівці'
      expect(page).to have_content search[:checkin].strftime('%d.%m.%Y')
      expect(page).to have_content search[:checkin].strftime('%d.%m.%Y')
    end

    scenario 'with invalid attributes' do
      expect(page).to have_content 'Booking'

      within '#searchBooking' do
        fill_in 'search_city', with: search[:city]
        fill_in 'search_checkin', with: Date.current - 1.day
        fill_in 'search_checkout', with: search[:checkout]

        page.find('#searchSubmit').click
      end

      expect(page).to have_content 'Must be search from today'

      within '#searchBooking' do
        fill_in 'search_city', with: search[:city]
        fill_in 'search_checkin', with: search[:checkin]
        fill_in 'search_checkout', with: Date.current + 6.day

        page.find('#searchSubmit').click
      end

      expect(page).to have_content 'Must be at minimum 1 day and maximum 5 days'

      within '#searchBooking' do
        fill_in 'search_city', with: nil
        fill_in 'search_checkin', with: search[:checkin]
        fill_in 'search_checkout', with: search[:checkout]

        page.find('#searchSubmit').click
      end
      expect(page).to have_content 'Booking'
    end
  end
end
