require 'rails_helper'

RSpec.describe SearchesController, type: :controller do
  describe 'GET #new' do
    before { get :new }

    it 'assigns new search to @search' do
      expect(assigns(:search)).to be_a_new(Search)
    end

    it 'renders view new' do
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    let(:params) do
      { search: attributes_for(:search) }
    end
    let(:invalid_params) do
      { search: attributes_for(:wrong_search) }
    end

    context 'with valid attributes' do
      it 'saves the search to database' do
        expect { post :create, params: params }.to change(Search, :count).by(1)
      end

      it 'redirects to show view' do
        post :create, params: params
        expect(response).to redirect_to search_searches_path(assigns(:search))
      end
    end

    context 'with invalid attributes' do
      it 'does not save the search to database' do
        expect { post :create, params: invalid_params }
          .to_not change(Search, :count)
      end

      it 'renders new view' do
        post :create, params: invalid_params
        expect(response).to render_template :new
      end
    end
  end

  describe 'GET #show' do
    let(:search) { create(:search) }

    before { get :show, params: { id: search.id } }

    it 'assigns search to @search' do
      expect(assigns(:search)).to eq(search)
    end

    it 'renders search view' do
      expect(response).to render_template :show
    end
  end

  describe 'GET #results' do
    let(:parsed) { JSON.parse(response.body) }

    describe 'state completed' do
      let!(:search) { create(:search, state: 'completed') }

      before { get :results, params: { id: search.id }, json: true }
      it 'response status ready' do
        expect(parsed['status']).to eq('ready')
      end
    end

    describe 'state processing' do
      let!(:search) { create(:search, state: 'nil') }

      before { get :results, params: { id: search.id }, json: true }
      it 'response status not_ready' do
        expect(parsed['status']).to eq('not_ready')
      end
    end
  end
end
