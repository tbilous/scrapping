require 'rails_helper'

RSpec.describe Search, type: :model do
  it { should validate_presence_of :city }

  it do
    should allow_values(Date.current)
      .for(:checkin)
  end

  it do
    should_not allow_values(Date.current - 1)
      .for(:checkin)
  end

  it do
    should_not allow_values(Date.current + 6)
      .for(:checkout)
  end

  let(:search) { create(:search, state: 'running') }

  it 'should queue worker for getting job' do
    expect { search.run_callbacks(:commit) }.to change { SearchWorker.jobs.count }.by(1)
  end
end
