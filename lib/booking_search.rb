require 'open-uri'
require 'nokogiri'
require 'mechanize'

# img, - photo
# :title, - name
# :district, - district of place
# :stars,   - stars
# :price,   - price
# :reinforcement, - breakfast, free cancel, etc
# :link,    - link to hotel on booking.com
# :avg_score  - customer rating
# :days  - count nights
# example: BookingSearch.perform("Київ", Date.current, Date.current + 1)

module BookingSearch
  extend self

  Entry = Struct.new(:img,
                     :title,
                     :district,
                     :stars,
                     :price,
                     :reinforcement,
                     :link,
                     :avg_score,
                     :days
  )

  def perform(city, checkin, checkout)
    @timeline = timeline(checkin, checkout)
    home_ua = agent.get('http://www.booking.com//index.uk-ua.html?sb_price_type=total;=;lang=uk-ua;lang_click=top;cdl=uk-ua')
    home_en = agent.get('http://www.booking.com//index.en-us.html?sb_price_type=total;=;lang=en-us;lang_click=top;cdl=en-us')

    home = home_ua
    search_results = search(home, city, checkin, checkout)
    # wait for lazy load
    sleep(2)
    parse_pages(search_results)
  end

  def search(page, city, checkin, checkout)
    search_form = page.form_with(:dom_id => 'frm')

    search_form.ss = city
    search_form.checkin_month = checkin.month
    search_form.checkin_monthday = checkin.day
    search_form.checkin_year = checkin.year
    search_form.checkout_month = checkout.month
    search_form.checkout_monthday = checkout.day
    search_form.checkout_year = checkout.year

    search_form.submit
  end

  def parse_pages(page)
    results = []
    while page.present? do
      results << parse_results(page.root.css('.sr_item'))

      next_page_link =  page.search('.paging-next').first rescue nil
      next_page_href =  'http://www.booking.com//' + next_page_link['href'].to_s rescue nil

      # make sure our access pattern has enough jitter
      # yes, sleep is too long but with random timeouts i'll cover my ass
      sleep(0.3 + rand(2) + rand) if next_page_href.present?

      # If you wont to enable remote pagination please uncomment next text string and remove 'page=nil'
      page = next_page_href.present? ? agent.get(next_page_href) : nil
      # page = nil
    end

    results.flatten.map(&:to_h)
  end

  def parse_results(entries)
    entries.map do |entry|
      # noinspection RubyArgCount
      Entry.new img = entry.css('.hotel_image').attr('src').value,
                title = entry.css('span.sr-hotel__name').text.strip,
                district =entry.css('.district_link').text,
                stars = clear_text(entry.css('.stars').css('.invisible_spoken').text),
                price = clear_price(entry.css('.roomPrice').css('.price').text.strip),
                reinforcement = clear_text(entry.css('.roomPrice').css('.sr_room_reinforcement')
                             .text.strip.gsub("\n", ' ').squeeze('')),
                link ='http://www.booking.com' + entry.css('.hotel_name_link').attr('href').to_s,
                avg_score = mb_float(entry.css('.sr_review_score').css('.average').text),
                days = @timeline
    end
  end

  def agent
    Mechanize.new do |agent|
      agent.user_agent_alias = 'Mac Safari'
    end
  end

  def clear_text(val)
    val.present? ? val : nil
  end

  def clear_price(val)
    val.present? ? val.gsub(/[^\d\.]+/, '').to_f : nil
  end

  def timeline(start, finish)
    average = (finish - start).to_i
    average == 0 ? average + 1 : average
  end

  def mb_float(val)
    val.present? ? val.to_f : nil
  end
end
