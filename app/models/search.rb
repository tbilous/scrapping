class Search < ApplicationRecord
  validates :city, presence: true
  validates_date :checkin, on_or_after: :today,
                           on_or_after_message: 'Must be search from today'
  validates_date :checkout, between: [(Date.current + 1), (Date.current + 5.days)],
                            on_or_after_message: 'Must be at minimum 1 day and maximum 5 days',
                            on_or_before_message: 'Must be at minimum 1 day and maximum 5 days'

  after_commit :booking, on: :create

  def filter_by_price
    results = self.results.select { |item| !item['price'].nil? }.sort_by { |h| -(h['price']) }
    results
  end

  def booking
    SearchWorker.schedule(id)
  end
end
