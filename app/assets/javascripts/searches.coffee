# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

timeout = 300
timer = null

ready = ->
  dataContainer = $('#searchData')
  dataContainer.html("<div id='loader'>Loading ...</div>")

  loadResults = (id) ->
    jQuery.get "/searches/#{id}/results.json", (data) ->
      if data.status == 'ready'
        data = data.results
        $(dataContainer).each ->
          $('.search-grid').detach()
        $(dataContainer).pagination
          dataSource: data
          pageSize: 20
          ulClassName: 'pagination-menu'
          callback: (data, pagination) ->
            for i in data
              $(dataContainer).prepend App.utils.render('booking', i)
            return
        for i in data
          $(dataContainer).prepend App.utils.render('booking', i)
        clearTimeout(timer)
        $('#loader').detach()

  if $(dataContainer).length
    pollResults = (id) ->
      loadResults(id)

  id = dataContainer.data('id')
  timer = setInterval(pollResults, timeout, id)

$(document).on("turbolinks:load", ready)

#$('#demo').pagination
#  dataSource: []
#  pageSize: 5
#  showPrevious: false
#  showNext: false
#  callback: (data, pagination) ->
## template method of yourself
#    html = template(data)
#    dataContainer.html html
#    return
