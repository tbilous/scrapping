require 'booking_search'

class SearchWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 3

  def self.schedule(id)
    SearchWorker.perform_async(id)
  end

  def perform(search_id)
    search = Search.find(search_id)
    search.update_attribute(:state, :in_progress)

    begin
      res = BookingSearch.perform(search.city, search.checkin, search.checkout)
      search.update_attributes(state: :completed, results: res)
    rescue => e
      search.update_attribute(:state, :failed)
      raise e
    end

    :ok
  end
end
