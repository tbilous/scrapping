class SearchesController < ApplicationController
  def new
    @search = Search.new
  end

  def create
    @search = Search.create(strong_params)
    if @search.save
      redirect_to action: :show, id: @search.id
    else
      render 'new'
    end
  end

  def show
    @search = Search.find(params[:id])
  end

  def results
    search = Search.find(params[:id])

    if search.state == 'completed'
      render json: { status: :ready,
                     results: search.filter_by_price }
    else
      render json: { status: :not_ready }
    end
  end

  private

  def strong_params
    params.require(:search).permit(:city, :checkin, :checkout)
  end
end
