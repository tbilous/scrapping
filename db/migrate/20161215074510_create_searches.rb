class CreateSearches < ActiveRecord::Migration[5.0]
  def change
    create_table :searches do |t|
      t.string :city
      t.date :checkin
      t.date :checkout
      t.string :state
      t.json :results

      t.timestamps
    end
  end
end
